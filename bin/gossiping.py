#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from lxml import html
from lxml import etree
import json
import requests

ptt = 'https://www.ptt.cc'

def latest_page(session, ptt):
  res = session.get('{}/bbs/Gossiping/index.html'.format(ptt))
  tree = html.fromstring(res.content)
  form = tree.xpath('/html/body/div[2]/form')[0]
  form_action = form.action
  hidden_input = tree.xpath('/html/body/div[2]/form/input')[0]
  hidden_from_value = hidden_input.value
  button = tree.xpath('/html/body/div[2]/form/div[1]/button')[0]
  button_yes_value = button.attrib['value']
  data = {
    'from': hidden_from_value,
    'yes': button_yes_value
  }
  ask_over18_url = '{}{}'.format(ptt, form_action)
  response = session.post(ask_over18_url, data = data, cookies = session.cookies)
  return response.content

def prev_page(tree):
  prev_page = tree.xpath('/html/body/div[2]/div[1]/div/div[2]/a[2]')[0]
  return prev_page.attrib['href']

def extract_links(tree):
  hrefs = []
  divs = tree.xpath('/html/body/div[2]/div[2]/div')
  for div in divs:
    link = div.xpath('div[2]/a')
    for e in link:
      hrefs.append(e.attrib['href'])
  return hrefs

def extract_content(session, links):
  for link in links:
    url = "{}{}".format(ptt, link)
    print(url)
    res = session.get(url)
    tree = html.fromstring(res.content)
    author       = tree.xpath('//div[1]/div[1]/span[2]/text()')[0]
    subject      = tree.xpath('//div[1]/div[3]/span[2]/text()')[0]
    timestamp    = tree.xpath('//div[1]/div[4]/span[2]/text()')[0]
    content      = tree.xpath('//*[@id="main-content"]/text()')[0]
    main_content = tree.xpath('//*[@id="main-content"]/text()')[0]
    f2           = tree.xpath('//span[@class="f2"]/text()')
    push         = tree.xpath('//div[@class="push"]')
    for div in push:
      for span in div:
        span.text

def extract(page): 
  tree = html.fromstring(page)
  links = extract_links(tree)
  extract_content(session, links)
  prev = prev_page(tree)
  url = '{}{}'.format(ptt, prev)
  prev_page_res = session.get(url)
  extract(prev_page_res.content)

if __name__ == "__main__":
  session = requests.session()
  latest = latest_page(session, ptt)
  extract(latest)
